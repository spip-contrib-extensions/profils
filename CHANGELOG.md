# Changelog

## Unreleased

- ...

## 2.3.0 - 2024-11-21

### Changed

- Compatibilité mini SPIP 4.1
- syntaxe simplifée des items de langue pour les fichiers sources
- #21 renommer les fonctions dépréciées `generer_info_entite` et `generer_info_entite`

## 2.2.0 - 2024-08-05

### Changed

- #17 La création automatique d'annuaires est optionnelle et désactivée par défaut.

## 2.1.0 - 2023-06-28

### Added

- `profils_profil_lire()` peut retourner un tableau multidimensionnel avec l'option `format` = `complet`

### Deprecated

- `profils_recuperer_infos_simples()` est renommée en `profils_profil_lire()`
- L'option `format_cles` de cette fonction est renommée en `format`

## 2.0.0 - 2023-04-13

### Added

- Ajout des coordonnées URLs (#15)

### Changed

- Compatibilité SPIP 4+