<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-profils?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// P
	'profils_description' => 'Mit diesem Plugin können Benutzerprofile mit unterschiedlichen Feldern angelegt werden (für natürliche Personen oder Organisationen, Adressen, etc). Profile können Benutzer-IDs zugeordnet werden.',
	'profils_nom' => 'Profile',
	'profils_slogan' => 'Nutzerprofile verwalten',
];
