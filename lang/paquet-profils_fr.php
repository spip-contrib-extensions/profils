<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/profils.git

return [

	// P
	'profils_description' => 'Ce plugin permet de créer des profils d’utilisateurs en configurant quelles informations ils peuvent contenir (une fiche de personne ou d’organisation, des coordonnées, etc). Ensuite il est possible de dire qu’un compte utilisateur utilise tel profil.',
	'profils_nom' => 'Profils',
	'profils_slogan' => 'Gérer des profils d’utilisateurs',
];
