<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/profils?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_previsu' => 'Preview',

	// C
	'configurer_email_unique_explication' => 'SPIP already forbid to sign up when an email is used in another account. In addition, this option forbid to change afterwards the email for one already used elsewhere, when we update a profile.',
	'configurer_email_unique_label_case' => 'Forbid the possibiliy of the same email in multiple accounts',
	'configurer_email_unique_prevenir_label_case' => 'Always warn that the account already exists, rather than replacing the information on the already existing profile.',
	'configurer_id_profil_defaut_label' => 'Default profile',
	'configurer_titre' => 'Profiles configuration',
	'configurer_utiliser_annuaires_co_non' => '(directories are currently disabled).',
	'configurer_utiliser_annuaires_co_oui' => '(directories are currently enabled).',
	'configurer_utiliser_annuaires_explication' => 'If directories are activated in the Contacts & Organizations plugin, then each profile will automatically have a directory assigned',
	'configurer_utiliser_annuaires_label_case' => 'Use directories',

	// E
	'erreur_autoriser_profil' => 'This account doesn’t exsit, or your don’t have the right to modify it',
	'erreur_email_unique' => 'This address is already used on an account. Please choose another one.',
	'erreur_email_unique_admin' => 'This email is already used <a href="@url@"> in an existing account </a>. Please enter another.',

	// M
	'message_erreur_importer_format' => 'Headers format is not supported.',
	'message_erreur_importer_jobs' => '@nb_jobs@ imports launched successfully BUT @nb_erreurs@ couldn’t be launched!',
	'message_ok_importer' => 'Launch of @nb@ imports of profils in the job list.',

	// P
	'profils_titre' => 'Profiles',
];
