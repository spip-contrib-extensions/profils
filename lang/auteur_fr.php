<?php

return [
 'champ_bio_label' => 'Descriptif',
 'champ_email_label' => 'Adresse email',
 'champ_nom_label' => 'Nom ou pseudo',
 'champ_pgp_label' => 'Clé PGP',
 'champ_nom_site_label' => 'Nom du site',
 'champ_url_site_label' => 'URL du site'
];
