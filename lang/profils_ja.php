<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/profils?lang_cible=ja
// ** ne pas modifier le fichier **

return [

	// C
	'configurer_email_unique_explication' => 'SPIPは既に他のアカウントで使われているメールアドレスでの新規登録をブロックします。このオプションは、プロファイルを更新する際、既にほかで使われているメールアドレスへあとから変更することも禁止します。',
	'configurer_email_unique_label_case' => '複数のアカウントで同じメールアドレスの使用を禁止する', # MODIF
	'configurer_id_profil_defaut_label' => 'デフォールトのプロファイル',
	'configurer_titre' => 'プロファイルの設定',

	// E
	'erreur_autoriser_profil' => 'このアカウントは存在しないか、修正する権限がありません',
	'erreur_email_unique' => 'このアドレスは他のアカウントで既に使用されています。他のアドレスを使用してください。',

	// P
	'profils_titre' => 'プロファイル',
];
