<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/profils?lang_cible=pt
// ** ne pas modifier le fichier **

return [

	// C
	'configurer_email_unique_explication' => 'O SPIP já bloqueia o registo de uma pessoa quando o e-mail é utilizado noutra conta. Esta opção também torna possível proibir a alteração subsequente de um e-mail para um que já é utilizado noutro local, quando se actualiza o perfil de uma pessoa.',
	'configurer_email_unique_label_case' => 'Proibir a utilização do mesmo correio electrónico em várias contas.',
	'configurer_email_unique_prevenir_explication' => 'Por omissão, quando criada por outra pessoa, se o e-mail já existir, a informação é substituída pela informação que acabou de ser enviada no perfil existente. Esta opção força um erro a ser exibido.',
	'configurer_email_unique_prevenir_label_case' => 'Advertir sempre que a conta já existe, em vez de sobrescrever a informação de perfil existente.',
	'configurer_id_profil_defaut_label' => 'Perfil por omissão',
	'configurer_titre' => 'Configuração de perfis',

	// E
	'erreur_autoriser_profil' => 'Esta conta não existe ou não tem o direito de a alterar',
	'erreur_email_unique' => 'Este endereço já está a ser utilizado numa conta existente. Por favor, queira introduzir outro.',
	'erreur_email_unique_admin' => 'Este endereço já está a ser utilizado  <a href="@url@">numa conta existente</a>. 
Por favor, queira introduzir outro.',

	// M
	'message_erreur_importer_format' => 'Formato do cabeçalho não suportado.',
	'message_erreur_importer_jobs' => '@nb_jobs@ importações lançadas com sucesso, MAS @nb_erreurs@ não puderam ser lançados!',
	'message_ok_importer' => 'Lançamento de @nb@ importações de perfis para a lista de trabalhos.',

	// P
	'profils_titre' => 'Perfis',
];
