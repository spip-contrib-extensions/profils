<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/profil?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// A
	'ajouter_lien_profil' => 'Agregar este perfil',

	// C
	'champ_config_activer_contact_explication_texte' => 'Activando al mismo tiempo informaciones de organización y de contacto, será siempre la organización la ficha principal ligada a la cuenta del utilizador, y el contacto estará ligado a la organización.',
	'champ_config_activer_contact_label_case' => 'Agregar informaciones de contacto (persona física)',
	'champ_config_activer_coordonnees_auteur_label_case' => 'Agregar datos a la cuennta del utilizador',
	'champ_config_activer_coordonnees_contact_label_case' => 'Agregar datos para el contacto',
	'champ_config_activer_coordonnees_organisation_label_case' => 'Agregar datos para la organización',
	'champ_config_activer_groupe_contact_label' => 'Agrupar los campos del contacto',
	'champ_config_activer_groupe_explication' => 'Si el campo está repleto se creará un gropo de campo coneste título',
	'champ_config_activer_groupe_organisation_label' => 'Agrupar los campos de la organización',
	'champ_config_activer_organisation_label_case' => 'Agrupar las informaciones de organizaciñon (persona moral)',
	'champ_config_auteur_caption' => 'Informaciones de la cuenta utilizador',
	'champ_config_champ_label' => 'Campo/objeto ligado',
	'champ_config_colonne_edition_label' => 'Edición',
	'champ_config_colonne_inscription_label' => 'Inscripción',
	'champ_config_colonne_obligatoire_label' => 'Obligatorio',
	'champ_config_contact_caption' => 'Informaciones de contacto',
	'champ_config_coordonnees_auteur_caption' => 'Datos de la cuenta utilizador',
	'champ_config_coordonnees_choix_sans_type' => 'Sin tipo',
	'champ_config_coordonnees_contact_caption' => 'Datos del contacto',
	'champ_config_coordonnees_explication' => 'Para agregar varios datos es necesario al menos una casilla marcada, si fuera necesario revalide el formulario.',
	'champ_config_coordonnees_label_label' => 'Etiqueta facultativa',
	'champ_config_coordonnees_organisation_caption' => 'Datos de la organización',
	'champ_config_coordonnees_type_label' => 'Tipo',
	'champ_config_id_annuaire_explication' => 'Usted activó la utilización de los anuarios múltiples. Si no escogió ningún anuario, se creará un anuarrio con el mismo nombre del perfil.',
	'champ_config_id_annuaire_label' => 'Qué anuario',
	'champ_config_objet_label' => 'Objeto',
	'champ_config_organisation_caption' => 'Informe de la organización',
	'champ_groupe_auteur_label' => 'Cuenta del utilizador',
	'champ_groupe_contact_label' => 'Contacto',
	'champ_groupe_organisation_label' => 'Organización',
	'champ_identifiant_label' => 'Nombre de usuario',
	'champ_titre_label' => 'Título',
	'comptes_aucun' => 'No hay cuenta de este perfil',
	'comptes_creer' => 'Crear una nueva cuenta para este perfil',
	'comptes_liste_detaille' => 'Ver la lista completa',
	'comptes_modifier' => 'Modificar una cuenta de perfil',
	'confirmer_supprimer_profil' => '¿confirma la eliminación de este perfil?',

	// E
	'envoyer_notification_label_case' => 'Enviar una notificación invitando a crear su clave',
	'erreur_email_obligatoire' => 'Usted debe inscribir obligatoriamente un mail en los campos o en los datos.',
	'errreur_identifiant_existant' => 'Este nombre de usuario ya existe',
	'exporter_champ_seulement_colonnes_label_case' => 'No exportar los datos, solo las columnas',
	'exporter_titre' => 'Exportación',

	// I
	'icone_creer_profil' => 'Crear un perfil',
	'icone_modifier_profil' => 'Modificar este perfil',
	'importer_titre' => 'Importación',
	'info_1_profil' => 'Un perfil',
	'info_1_profil_compte' => 'Una cuenta de este perfil', # MODIF
	'info_aucun_profil' => 'No hay perfil',
	'info_nb_profil_comptes' => '@nb@ cuentas de este perfil', # MODIF
	'info_nb_profils' => '@nb@ perfiles',
	'info_profils_auteur' => 'Los perfiles de este autor',

	// N
	'notification_motdepasse_texte' => 'Se ha creado una nueva cuenta para usted en el sitio @nom_site_spip@ (@adresse_site@) 

Vaya a la dirección sigguiente: 

    @sendcookie@ 

Usted podra definir una clave y conectarse al sitio con su mail @email@.',
	'notification_motdepasse_titre' => 'Nueva cuenta',

	// R
	'retirer_lien_profil' => 'Retirar este perfil',
	'retirer_tous_liens_profils' => 'Retirar todos los perfiles',

	// S
	'supprimer_profil' => 'Suprimir este perfil',

	// T
	'texte_ajouter_profil' => 'Agregar un perfil',
	'texte_changer_statut_profil' => 'Este perfil es:',
	'texte_creer_associer_profil' => 'Crear y asociar un perfil',
	'texte_definir_comme_traduction_profil' => 'Este perfil es una traducción del perfil número:',
	'titre_langue_profil' => 'Idioma de este perfil',
	'titre_logo_profil' => 'Logo de este perfil',
	'titre_objets_lies_profil' => 'Ligados a este perfil',
	'titre_profil' => 'Perfil',
	'titre_profils' => 'Perfiles',
	'titre_profils_rubrique' => 'Perfiles de la rúbrica',
	'tri_croissant' => 'Selección creciente',
	'tri_decroissant' => 'Selección decreciente',
];
