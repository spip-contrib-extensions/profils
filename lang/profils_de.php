<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/profils?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'configurer_email_unique_explication' => 'SPIP unterbindet die Anmeldung mit einer bereits verwendeten Mailadresse. Mit dieser Option kann zusätzlich die nachträgliche Änderung der Mailadresse eines Profils verhindert werden, wenn  diese bereits in einem anderen Kontext verwendet wird.',
	'configurer_email_unique_label_case' => 'Mehrere Konten mit der selben Mailadresse unterbinden', # MODIF
	'configurer_id_profil_defaut_label' => 'Standardprofil',
	'configurer_titre' => 'Profile konfigurieren',

	// E
	'erreur_autoriser_profil' => 'Dieses Konto existiert nicht oder sie dürfen es nicht bearbeiten.',
	'erreur_email_unique' => 'Diese Mailadresse wird bereits verwendet. Bitte geben sie eine andere an.',

	// P
	'profils_titre' => 'Profile',
];
