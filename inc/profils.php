<?php
/**
 * Fonctions utilitaires pour les profils
 *
 * @plugin     Profils
 * @copyright  2018
 * @author     Les Développements Durables
 * @licence    GNU/GPL
 * @package    SPIP\Profils\Utils
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('base/objets');
include_spip('base/abstract_sql');
include_spip('action/editer_liens');
include_spip('inc/saisies');

/**
 * Trouver quel profil utiliser suivant s'il y a un auteur ou pas et s'il a déjà un profil
 * 
 * @param int|string $id_ou_identifiant_profil 
 * @param int $id_auteur 
 * @return int|string
 * 		Retourne un identifiant entier ou texte d'un profil
 */
function profils_selectionner_profil($id_ou_identifiant_profil='', $id_auteur=0) {
	$id_auteur = intval($id_auteur);
	
	// S'il y a un utilisateur et qu'il a déjà un profil sélectionné
	// c'est prioritaire au profil par défaut donc si pas d'autre profil demandé explicitement
	if (!$id_ou_identifiant_profil and $id_auteur > 0 and $id_profil = sql_getfetsel('id_profil', 'spip_auteurs', 'id_auteur = '.$id_auteur)) {
		$id_ou_identifiant_profil = $id_profil;
	}
	
	return $id_ou_identifiant_profil;
}

/**
 * Cherche le profil suivant un id SQL ou un identifiant textuel
 * 
 * @param int|string $id_ou_identifiant_profil
 * 		ID SQL ou identifiant textuel du profil
 * @return array|bool
 * 		Retourne le profil demandé ou false
 */
function profils_recuperer_profil($id_ou_identifiant_profil) {
	static $profils = array();
	$profil = false;
	
	// Si on l'a déjà trouvé avant
	if (isset($profils[$id_ou_identifiant_profil])) {
		return $profils[$id_ou_identifiant_profil];
	}
	// Sinon on cherche
	else {
		// Si pas de profil précis demandé
		if (!$id_ou_identifiant_profil) {
			include_spip('inc/config');
			// Si on a configuré un profil par défaut explicitement
			if ($id_profil_defaut = intval(lire_config('profils/id_profil_defaut'))) {
				$profil = sql_fetsel('*', 'spip_profils', 'id_profil = '.$id_profil_defaut);
			}
			// Sinon on prend le tout premier
			else {
				$profil = sql_fetsel('*', 'spip_profils', '', '', 'id_profil asc', '0,1');
			}
		}
		// Si c'est un identifiant numérique
		elseif (is_numeric($id_ou_identifiant_profil)) {
			$profil = sql_fetsel('*', 'spip_profils', 'id_profil = '.intval($id_ou_identifiant_profil));
		}
		// Si c'est un identifiant textuel
		else {
			$profil = sql_fetsel('*', 'spip_profils', 'identifiant = '.sql_quote($id_ou_identifiant_profil));
		}
	}
	
	if (isset($profil['config'])) {
		$profil['config'] = unserialize($profil['config']);
	}
	
	$profils[$id_ou_identifiant_profil] = $profil;
	return $profil;
}


/**
 * Cherche dans une config s'il y a bien un champ email obligatoire et si oui lequel
 * 
 * La fonction cherche l'email obligatoire *le plus proche* du compte utilisateur
 * 
 * @param array $config
 * 		Le tableau de configuration d'un profil
 * @return
 * 		Retourne le champ d'email principal
 */
function profils_chercher_champ_email_principal($config) {
	$champ = false;
	
	foreach (array('auteur', 'organisation', 'contact') as $objet) {
		if ($objet == 'auteur' or (defined('_DIR_PLUGIN_CONTACTS') and ($config["activer_$objet"] ?? null))) {
			// On cherche dans les champs de l'objet
			if (
				!$champ
				and $config[$objet]['email'] ?? null
				and in_array('inscription', $config[$objet]['email'])
				and in_array('obligatoire', $config[$objet]['email'])
			) {
				$champ = array($objet, 'email');
			}
			// Sinon on cherche dans les coordonnées emails
			elseif (
				!$champ
				and defined('_DIR_PLUGIN_COORDONNEES')
				and $config["activer_coordonnees_$objet"] ?? null
				and $config['coordonnees'][$objet]['emails']
			) {
				// On parcourt les emails configurés
				foreach ($config['coordonnees'][$objet]['emails'] as $champ_email) {
					if ($champ_email['inscription'] and $champ_email['obligatoire']) {
						$type = $champ_email['type'] ? $champ_email['type'] : 0;
						$champ = array('coordonnees', $objet, 'emails', $type, 'email');
					}
				}
			}
		}
	}
	
	return $champ;
}

/**
 * Cherche les saisies d'édition d'un objet ET des champs extras ajoutés
 * 
 * @param string $objet
 * 		Nom de l'objet dont on cherche les saisies
 * @return array
 * 		Retoure un tableau de saisies
 * 
 */
function profils_chercher_saisies_objet($objet, $args=array('new')) {
	static $saisies = array();
	$objet = objet_type($objet);
	$id_objet = $args[0]; // L'id de l'objet est toujours en premier
	
	if (isset($saisies[$objet][$id_objet])) {
		return $saisies[$objet][$id_objet];
	}
	else {
		$saisies[$objet][$id_objet] = array();
		
		// Les saisies de base
		if ($saisies_objet = saisies_chercher_formulaire("editer_$objet", $args)) {
			$saisies[$objet][$id_objet] = array_merge($saisies[$objet][$id_objet], $saisies_objet);
		}
		
		// Les saisies des champs extras
		if (defined('_DIR_PLUGIN_CEXTRAS')) {
			include_spip('cextras_pipelines');
			
			if ($saisies_extra = champs_extras_objet(table_objet_sql($objet))) {
				$saisies[$objet][$id_objet] = array_merge($saisies[$objet][$id_objet], $saisies_extra);
			}
		}
	}
	
	return $saisies[$objet][$id_objet];
}

/**
 * Cherche les saisies configurées pour un profil et pour tel formulaire (inscription ou édition)
 * 
 * @param string $form
 * 		Quel formulaire : "inscription" ou "edition" 
 * @param $id_auteur 
 * 		Identifiant du compte utilisateur dont on cherche les saisies
 * @param $id_ou_identifiant_profil 
 * 		Identifiant du profil, notamment si c'est une création
 * @return array
 * 		Retourne le tableau des saisies du profil
 */
function profils_chercher_saisies_profil($form, $id_auteur=0, $id_ou_identifiant_profil='') {
	$saisies = array();
	$noms_changes = array();
	
	// On cherche le bon profil
	$id_ou_identifiant_profil = profils_selectionner_profil($id_ou_identifiant_profil, $id_auteur);
	
	// Récupérer les objets liés au profil utilisateur
	$ids = profils_chercher_ids_profil($id_auteur, $id_ou_identifiant_profil);
	
	// On ne continue que si on a un profil sous la main
	if ($profil = profils_recuperer_profil($id_ou_identifiant_profil) and $config = $profil['config']) {
		// Pour chaque objet, on va chercher les bonnes saisies
		foreach (array('auteur', 'organisation', 'contact') as $objet) {
			// Si c'est autre chose que l'utilisateur, faut le plugin qui va avec et que ce soit activé
			if ($objet == 'auteur' or (defined('_DIR_PLUGIN_CONTACTS') and ($config["activer_$objet"] ?? null))) {
				// On récupère les champs pour cet objet ET ses champs extras s'il y a
				$cle_objet = id_table_objet($objet);
				$saisies_objet = profils_chercher_saisies_objet($objet, array($ids[$cle_objet]));
				$saisies_a_utiliser = array();
				
				// Pour chaque chaque champ vraiment configuré
				if (isset($config[$objet]) and is_array($config[$objet])) {
					foreach ($config[$objet] as $champ => $config_champ) {
						// On cherche la saisie pour ce champ SI c'est pour le form demandé
						if (in_array($form, $config_champ) and $saisie = saisies_chercher($saisies_objet, $champ)) {
							// On garde en mémoire le nom modifié pour les afficher_si
							$noms_changes[$saisie['options']['nom']] = $objet . '[' . $saisie['options']['nom'] . ']';
							// On modifie son nom
							$saisie['options']['nom'] = $objet . '[' . $saisie['options']['nom'] . ']';
							// On modifie son obligatoire suivant la config
							$saisie['options']['obligatoire'] = in_array('obligatoire', $config_champ) ? 'oui' : false;
							// On ajoute la saisie
							$saisies_a_utiliser[] = $saisie;
						}
					}
					
					// On transforme les noms déjà gardés dans les afficher_si
					foreach ($noms_changes as $nom => $changement) {
						$saisies_a_utiliser = saisies_transformer_option($saisies_a_utiliser, 'afficher_si', "#@$nom@#", "@$changement@");
					}
					$noms_changes = array();
				}
				
				// On cherche des coordonnées pour cet objet
				if (
					defined('_DIR_PLUGIN_COORDONNEES')
					and $config["activer_coordonnees_$objet"] ?? null
					and $coordonnees = $config['coordonnees'][$objet] ?? []
				) {
					// Pour chaque type de coordonnéees (num, email, adresse, coordonnees_url)
					foreach ($coordonnees as $coordonnee => $champs) {
						// Pour chaque champ ajouté
						foreach ($champs as $cle => $champ) {
							// Si ce cette coordonnées est configurée pour le form demandé
							if (
								$champ[$form]
								and isset($champ['type'])
							) {
								$cle_objet = id_table_objet($coordonnee);
								$id_objet = $ids['coordonnees'][$objet][$coordonnee][$champ['type']];
								$args_editer = array($id_objet);
								if ($coordonnee == 'adresses') {
									// C'EST DE LA MERDE
									$args_editer = array($id_objet, '', '', '', '', '', '', $champ['obligatoire'] ?? null);
								}
								// Attention, si pas de type, on transforme ici en ZÉRO
								if (!$champ['type']) {
									$champ['type'] = 0;
								}
								// On va chercher les saisies de ce type de coordonnées
								$saisies_coordonnee = profils_chercher_saisies_objet($coordonnee, $args_editer);
								// On vire le titre libre dans tous les cas
								$saisies_coordonnee = saisies_supprimer($saisies_coordonnee, 'titre');
								// On change le nom de chacun des champs
								$saisies_coordonnee =  saisies_transformer_noms(
									$saisies_coordonnee,
									'/^\w+$/',
									"coordonnees[$objet][$coordonnee][${champ['type']}][\$0]"
								);
								$saisies_coordonnee = saisies_transformer_option(
									$saisies_coordonnee,
									'afficher_si',
									'/@(\w+)@/',
									"@coordonnees[$objet][$coordonnee][${champ['type']}][\$1]@"
								);
								// On reconstitue le label
								$label = $champ['label'] ? $champ['label'] : _T(objet_info(table_objet_sql($coordonnee), 'texte_objet'));
								if ($champ['type'] and !$champ['label']) {
									$label .= ' (' . coordonnees_lister_types_coordonnees(objet_type($coordonnee), $champ['type']) . ')';
								}
								// Pour les coordonnées "simples", on ne garde que le champ principal : numéro, email, url
								// On change peut-être aussi le label pour indiquer le type, et le obligatoire
								if (in_array($coordonnee, array('numeros', 'emails', 'coordonnees_urls'))) {
									$coordonnees_champs_principaux = [
										'email'           => 'email',
										'numero'          => 'numero',
										'coordonnees_url' => 'url',
									];
									$nom_champ_principal = $coordonnees_champs_principaux[objet_type($coordonnee)];
									$nom_saisie_principale = "coordonnees[$objet][$coordonnee][${champ['type']}][$nom_champ_principal]";
									$saisies_coordonnee = saisies_modifier(
										$saisies_coordonnee,
										$nom_saisie_principale,
										array(
											'options' => array(
												'label' => $label,
												'obligatoire' => (isset($champ['obligatoire']) and $champ['obligatoire']) ? 'oui' : false,
											),
										),
										true
									);
									// On supprime tous les autres champs
									foreach (array_keys(saisies_lister_par_nom($saisies_coordonnee) ?: []) as $nom_saisie) {
										if ($nom_saisie !== $nom_saisie_principale) {
											$saisies_coordonnee = saisies_supprimer($saisies_coordonnee, $nom_saisie);
										}
									}
									// On ajoute enfin
									$saisies_a_utiliser	= array_merge($saisies_a_utiliser, $saisies_coordonnee);
								}
								// Alors que si c'est une adresse on l'utilise pour le groupe de champs
								else {
									$saisies_a_utiliser[] = array(
										'saisie' => 'fieldset',
										'options' => array(
											'nom' => "groupe_${coordonnee}_$cle",
											'label' => $label,
										),
										'saisies' => $saisies_coordonnee,
									);
								}
							}
						}
					}
				}
				
				// On teste s'il faut un groupe de champs ou pas pour cet objet
				if (
					$saisies_a_utiliser
					and $legend = $config["activer_groupe_$objet"] ?? null
				) {
					$saisies[] = array(
						'saisie' => 'fieldset',
						'options' => array(
							'nom' => "groupe_$objet",
							'label' => $legend,
						),
						'saisies' => $saisies_a_utiliser,
					);
				}
				// Sinon on les ajoute directement
				else {
					$saisies = array_merge($saisies, $saisies_a_utiliser);
				}
			}
		}
		
		// Quel est le champ email principal
		$champ_email_principal = profils_chercher_champ_email_principal($config);
		$name_email_principal = '';
		foreach ($champ_email_principal as $cle) {
			if (!$name_email_principal) {
				$name_email_principal = $cle;
			}
			else {
				$name_email_principal .= "[$cle]";
			}
		}
		// On ajoute une info à ce champ
		if ($saisie_email_principal = saisies_chercher($saisies, $name_email_principal)) {
			$saisie_email_principal['options']['conteneur_class'] = ($saisie_email_principal['options']['conteneur_class'] ?? '') . ' email_principal';
			$saisies = saisies_modifier(
				$saisies,
				$name_email_principal,
				$saisie_email_principal,
				true
			);
		}
	}
		
	return $saisies;
}

/**
 * Récupérer tous les identifiants des objets liés au profil d'un compte utilisateur
 *
 * @param int $id_auteur=0
 * 		Identifiant d'un auteur précis, sinon visiteur en cours
 * @return array
 * 		Retourne un tableau de tous les identifiants
 */
function profils_chercher_ids_profil($id_auteur=0, $id_ou_identifiant_profil='') {
	$ids = array();
	$ids['id_auteur'] = intval($id_auteur);
	$coordonnees_liees = array();
	
	// On cherche le bon profil
	$id_ou_identifiant_profil = profils_selectionner_profil($id_ou_identifiant_profil, $ids['id_auteur']);
	
	// Si pas d'utilisateur, il faut en créer un
	if (!$ids['id_auteur']) {
		$ids['id_auteur'] = 'new';
	}
	
	// Maintenant on ne continue que si on a trouvé un profil
	if ($profil = profils_recuperer_profil($id_ou_identifiant_profil) and $config = $profil['config']) {
		// Si le plugin est toujours là
		if (defined('_DIR_PLUGIN_CONTACTS')) {
			// Est-ce qu'il y a une orga en fiche principale ?
			if ($config['activer_organisation'] ?? null) {
				// Cherchons une organisation
				if (
					!intval($ids['id_auteur'])
					or !$ids['id_organisation'] = intval(sql_getfetsel('id_organisation', 'spip_organisations', 'id_auteur = '.$ids['id_auteur']))
				) {
					$ids['id_organisation'] = 'new';
				}
				
				// Il peut aussi y avoir un contact physique *lié à l'orga*
				if (
					!intval($ids['id_organisation'])
					or !$liens = objet_trouver_liens(array('organisation'=>$ids['id_organisation']), array('contact'=>'*'))
					or !$contact = $liens[0]
					or !$ids['id_contact'] = intval($contact['id_objet'])
				) {
					$ids['id_contact'] = 'new';
				}
			}
			elseif ($config['activer_contact']) {
				// Cherchons un contact physique
				if (
					!intval($ids['id_auteur'])
					or !$ids['id_contact'] = intval(sql_getfetsel('id_contact', 'spip_contacts', 'id_auteur = '.$ids['id_auteur']))
				) {
					$ids['id_contact'] = 'new';
				}
			}
		}
		
		// Si on doit chercher des coordonnées
		if (defined('_DIR_PLUGIN_COORDONNEES')) {
			foreach (array('auteur', 'organisation', 'contact') as $objet) {
				// S'il y a des coordonnées activées pour cet objet
				if (
					$config["activer_coordonnees_$objet"] ?? null
					and $coordonnees = $config['coordonnees'][$objet] ?? []
				) {
					$cle_objet = id_table_objet($objet);
					
					foreach ($coordonnees as $coordonnee => $champs) {
						$cle_coordonnee = id_table_objet($coordonnee);
						
						foreach ($champs as $cle => $champ) {
							// On cherche s'il y a une liaison sinon new
							if (
								isset($champ['type'])
								and (
									!$id_objet = $ids[$cle_objet]
									or !$liens = objet_trouver_liens(
										array(objet_type($coordonnee) => '*'),
										array($objet => $id_objet),
										array('type = '.sql_quote($champ['type']))
									)
									or !$liaison = $liens[0]
									or !$coordonnees_liees[$objet][$coordonnee][$champ['type']] = $liaison[$cle_coordonnee]
								)
							) {
								$coordonnees_liees[$objet][$coordonnee][$champ['type']] = 'new';
							}
						}
					}
				}
			}
			
			// S'il y a des coordonnées on ajoute
			if ($coordonnees_liees) {
				$ids['coordonnees'] = $coordonnees_liees;
			}
		}
	}
	
	return $ids;
}

/**
 * Récupérer les informations complètes d'un compte de profil
 *
 * Renvoie un tableau multidimensionnel avec l'ensemble des valeurs
 * chargées par les formulaires d'édition des objets du profil,
 * chaque objet dans une clé (organisation, contact, etc.)
 *
 * @note
 * Fonction à usage interne, retourne des données sensibles telles que le login, etc.
 *
 * @param int $id_auteur=0
 * 		Identifiant d'un auteur précis, sinon visiteur en cours
 * @param string|int $id_ou_identifiant_profil
 *    Numéro ou identifiant d'un profil
 * @param array $options
 *    Options diverses
 *    - `quoi` pour indiquer dans quel contexte on récupère les infos.
 * @return array
 * 		Informations d'un profil, chaque objet SPIP dans une clé (organisation, contact, telephone, etc).
 */
function profils_recuperer_infos($id_auteur = 0, $id_ou_identifiant_profil = '', $options = array()) {
	include_spip('inc/editer');
	$retour = '';
	$infos = array();
	
	// Récupérer les objets liés au profil utilisateur
	extract(profils_chercher_ids_profil($id_auteur, $id_ou_identifiant_profil));
	
	// On charge les infos du compte SPIP
	$infos['auteur'] = formulaires_editer_objet_charger('auteur', $id_auteur, 0, 0, $retour, '');
	
	// On charge les infos de l'organisation
	if (isset($id_organisation) and $id_organisation) {
		$infos['organisation'] = formulaires_editer_objet_charger('organisation', $id_organisation, 0, 0, $retour, '');
		unset($infos['organisation']['_pipeline']);
	}
	
	// On charge les infos du contact
	if (isset($id_contact) and $id_contact) {
		$infos['contact'] = formulaires_editer_objet_charger('contact', $id_contact, intval($id_organisation ?? 0), 0, $retour, '');
	}
	
	// S'il y a des coordonnées
	if (isset($coordonnees) and $coordonnees and is_array($coordonnees)) {
		foreach ($coordonnees as $objet => $coordonnees_objet) {
			foreach ($coordonnees_objet as $coordonnee => $champs) {
				foreach ($champs as $type => $id) {
					// Attention, si pas de type, on transforme ici en ZÉRO
					if (!$type) {
						$type = 0;
					}
					$infos['coordonnees'][$objet][$coordonnee][$type] = formulaires_editer_objet_charger(objet_type($coordonnee), $id, 0, 0, $retour, '');
				}
			}
		}
	}

	// Pipeline pour les plugins qui ajoutent des saisies
	$infos = pipeline(
		'profils_recuperer_infos',
		array(
			'args' => array(
				'id_auteur' => $id_auteur,
				'id_ou_identifiant_profil' => $id_ou_identifiant_profil,
				'options' => $options,
			),
			'data' => $infos,
		)
	);

	return $infos;
}

/**
 * Lire un compte de profil
 *
 * Retourne un tableau contenant toutes les valeurs d'un profil.
 *
 * Plusieurs formats sont possibles, retournant un tableau simple ou multidimensionnel.
 * Par défaut c'est un tableau simple avec les clés au format label humain.
 *
 * Pour générer une vue d'un profil au moyen des saisies, utiliser le format `complet`.
 *
 * Exemples de retours selon les formats :
 *
 * @example
 * Format `simple` :
 * ```
 * array(
 *   'auteur_nom' => 'Lorem',
 *   'auteur_prenom' => 'Ipsum',
 * )
 * ```
 *
 * @example
 * Format `brut` :
 * ```
 * array(
 *   'auteur[nom]' => 'Lorem',
 *   'auteur[prenom]' => 'Ipsum',
 * )
 * ```
 *
 * @example
 * Format `label` :
 * ```
 * array(
 *   'Nom' => 'Lorem',
 *   'Prénom' => 'Ipsum',
 * )
 * ```
 *
 * @example
 * Format `complet` :
 * ```
 * array(
 *   'auteur' => array(
 *     'nom' => 'Lorem',
 *     'prenom' => 'Ipsum',
 *   )
 * )
 * ```
 *
 * @uses profils_recuperer_infos()
 * @uses saisies_chercher_formulaire()
 *
 * @param int $id_auteur
 *   Identifiant d'un auteur précis, par défaut le visiteur en cours
 * @param int|string $id_ou_identifiant_profil
 *   Numéro ou identifiant d'un profil
 * @param array $options
 *   Tableau d'options :
 *   - format (string) : format du tableau retourné, défaut = simple
 *     - simple  : tableau simple avec en clés les noms des saisies slugifiés → `auteur_truc_muche`
 *     - label   : tableau simple avec en clés des labels humains
 *     - brut    : tableau simple avec en clés les noms bruts des saisies → `auteur[truc][muche]`
 *     - complet : tableau multidimensionnel
 *   - lang (string) : pour forcer une langue si on utilise les labels humains
 *   - filtrer_vides (bool) : retirer les champs évalués à empty, défaut = false
 *   - @deprecated format_cles : utiliser `format`
 * @return array
 *   Tableau à 1 ou plusieurs niveaux.
 *   Voir les exemples pour tous les formats possibles.
 */
function profils_profil_lire($id_auteur = 0, $id_ou_identifiant_profil = '', $options = array()) {
	$profil = array();

	// Les options
	$format_defaut = 'simple';
	$options_defaut = array(
		'format'        => $format_defaut,
		'filtrer_vides' => false,
	);
	$options = array_merge($options_defaut, $options);
	// Options dépréciées
	if (isset($options['format_cles'])) {
		$options['format'] = $options['format_cles'];
		unset($options['format_cles']);
	}

	// On récupère les infos complètes
	$options_infos = array('quoi' => 'infos_simples');
	if ($infos_completes = profils_recuperer_infos($id_auteur, $id_ou_identifiant_profil, $options_infos)) {
		include_spip('inc/saisies');
		include_spip('inc/filtres');

		// Change temporairement la langue s'il y en a une donnée dans les options
		$lang = $GLOBALS['spip_lang'];
		$changer_lang = (!empty($options['lang']) and ($options['lang'] !== $lang));
		if ($changer_lang) {
			include_spip('inc/lang');
			changer_langue($options['lang']);
		}

		// Les saisies nous donne la liste des "vrais" champs du profil
		$saisies = saisies_chercher_formulaire('profil', array($id_auteur, $id_ou_identifiant_profil));
		$saisies_par_nom = saisies_lister_par_nom($saisies);
		foreach ($saisies_par_nom as $nom => $saisie) {
			$valeur = saisies_request($nom, $infos_completes);

			// Ignorer optionnellement les champs vides
			$ajouter = (empty($options['filtrer_vides']) or (!empty($options['filtrer_vides']) and $valeur));
			if ($ajouter) {

				// Plusieurs formats possibles
				$formats_possibles = array('simple', 'brut', 'label', 'complet');
				$format = (in_array($options['format'], $formats_possibles) ? $options['format'] : $format_defaut);
				switch ($format) {
					// simple : tableau simple
					// clés = nom de la saisie slugifié → truc_muche_0_bidule
					case 'simple':
						$cle = str_replace(array('[',']'), array('_',''), $nom);
						$profil[$cle] = $valeur;
						break;
					// brut : tableau simple
					// clés  = nom de la saisie → truc[muche][0][bidule]
					case 'brut':
						$cle = $nom;
						$profil[$cle] = $valeur;
						break;
					// label : tableau simple
					// clés = label de la saisie → 'Le truc du muche de chez bidule'
					case 'label':
						$cle = (isset($saisie['options']['label']) ? textebrut($saisie['options']['label']) : $nom);
						$profil[$cle] = $valeur;
						break;
					// complet : tableau multidimensionnel
					case 'complet':
						$profil = profils_nom_saisie_vers_tableau($nom, $valeur, $profil);
						break;
				}
			}
		}

		// Remettre la langue d'origine
		if ($changer_lang) {
			changer_langue($lang);
		}
	}

	return $profil;
}

/**
 * @deprecated v2.1.0 Use profils_profil_lire
 *
 * @return array
 */
function profils_recuperer_infos_simples(...$args) {
	// TODO : trigger un log deprecated en SPIP 5
	return profils_profil_lire(...$args);
}

/**
 * Peuple un tableau à partir d'un nom de saisie et de sa valeur
 *
 * Si on a une saisie nommée `[auteur][nom]`,
 * c'est comme si on faisait `$tableau['auteur']['nom'] = $valeur;`,
 * mais sans utiliser eval().
 *
 * @param string $nom
 *   Nom de la saisie
 * @param mixed $valeur
 *   Valeur de la saisie
 * @param array $tableau
 *   Tableau que l'on souhaite peupler
 * @return array
 */
function profils_nom_saisie_vers_tableau($nom, $valeur, $tableau) {
	// truc[muche] → [truc][muche]
	$pos = strpos($nom, '[');
	$notation = '[' . ($pos !== false ? substr_replace($nom, '][', $pos, 1) : $nom . ']');
	// Remove the opening and closing brackets
	$notation = trim($notation, '[]');
	// Split the notation by '][' to get individual keys
	$keys = explode('][', $notation);
	// Create the resulting array
	$tableau_tmp = &$tableau;
	foreach ($keys as $k => $key) {
		// Value = existing nested array
		if (isset($tableau_tmp[$key])) {
			$value = $tableau_tmp[$key];
		// or new nested array
		} elseif ($k < count($keys)-1) {
			$value = [];
		// or current value
		} else {
			$value = $valeur;
		}
		// Create a nested array for each key
		$tableau_tmp[$key] = $value;
		$tableau_tmp = &$tableau_tmp[$key];
	}

	return $tableau;
}

function profils_traiter_peupler_request($form, $champs_objet, $config_objet) {
	if (is_array($champs_objet) and $config_objet) {
		foreach ($config_objet as $champ => $config_champ) {
			// D'abord on vide ce champ dans l'environnement, si jamais un autre objet avait le même avant
			set_request($champ, null);
			
			// Si c'est configuré pour ce formulaire
			if (in_array($form, $config_champ)) {
				set_request('cextra_'.$champ, 1); // pour que champs extras le gère dans pre_edition ensuite
				
				if (isset($champs_objet[$champ])) {
					set_request($champ, $champs_objet[$champ]);
				}
			}
		}
	}
}

/**
 * @brief 
 * @param $id_auteur 
 * @param $id_ou_identifiant_profil 
 * @returns 
 */
function profils_enregistrer_profil($id_auteur, $id_ou_identifiant_profil='') {
	
}
