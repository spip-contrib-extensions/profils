<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_configurer_profils_saisies_dist() {
	$utiliser_annuaires_co = lire_config('contacts_et_organisations/utiliser_annuaires');
	$saisies = array(
		array(
			'saisie' => 'profils',
			'options' => array(
				'nom' => 'id_profil_defaut',
				'label' => _T('profils:configurer_id_profil_defaut_label'),
				'defaut' => lire_config('profils/id_profil_defaut'),
			),
		),
		array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'email_unique',
				'label_case' => _T('profils:configurer_email_unique_label_case'),
				'explication' => _T('profils:configurer_email_unique_explication'),
				'conteneur_class' => 'pleine_largeur',
				'defaut' => lire_config('profils/email_unique'),
			),
		),
		array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'email_unique_prevenir',
				'label_case' => _T('profils:configurer_email_unique_prevenir_label_case'),
				'explication' => _T('profils:configurer_email_unique_prevenir_explication'),
				'conteneur_class' => 'pleine_largeur',
				'defaut' => lire_config('profils/email_unique_prevenir'),
				'afficher_si' => '@email_unique@ != ""',
			),
		),
		// Est-ce qu'on veut créer et attribuer automatiquement des annuaires pour chaque profil ?
		array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'utiliser_annuaires',
				'label_case' => _T('profils:configurer_utiliser_annuaires_label_case'),
				'explication' => 
					_T('profils:configurer_utiliser_annuaires_explication')
					. ' ' . ($utiliser_annuaires_co ? _T('profils:configurer_utiliser_annuaires_co_oui') : _T('profils:configurer_utiliser_annuaires_co_non')),
				'conteneur_class' => 'pleine_largeur',
				'defaut' => lire_config('profils/utiliser_annuaires'),
			),
		),
	);
	
	return $saisies;
}
