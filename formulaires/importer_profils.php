<?php
/**
 * Gestion du formulaire de d'importer de profils depuis un tableur
 *
 * @plugin     Profils
 * @copyright  2018
 * @author     Les Développements Durables
 * @licence    GNU/GPL
 * @package    SPIP\Profils\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_importer_profils_saisies_dist($id_profil) {
	$saisies = array(
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'fichier',
				'type' => 'file',
				'label' => 'CSV',
				'obligatoire' => 'oui',
				'pleine_largeur' => 'oui',
			),
		),
		array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'envoyer_notification',
				'label_case' => _T('profil:envoyer_notification_label_case'),
				'pleine_largeur' => 'oui',
			),
		),
		'options' => array(
			'texte_submit' => _T('bouton_upload'),
			'inserer_debut' => '<h3 class="titrem">'._T('profil:importer_titre').'</h3>'
		),
	);

	return $saisies;
}

function formulaires_importer_profils_traiter_dist($id_profil) {
	$retours = array();
	$jobs = 0;
	$jobs_erreurs = 0;
	$erreur = '';
	
	$fichier = $_FILES['fichier']['tmp_name'];
	$importer_csv = charger_fonction('importer_csv', 'inc/');
	$envoyer_notification = _request('envoyer_notification') ? true : false;
	
	// Si on récupère bien un tableau
	if (is_array($profils = $importer_csv($fichier, true))) {

		// L'import n'est possible que si les colonnes sont au format `simple`, avec des underscores.
		include_spip('inc/profils');
		$colonnes = array_keys($profils[0]);
		$format = profils_detecter_format_colonnes($colonnes, $id_profil);
		$format_ok = ($format === 'simple');

		if ($format_ok) {
			foreach ($profils as $profil) {
				// On cherche rapidement un truc pertinent à mettre
				if (isset($profil['auteur_nom']) and $profil['auteur_nom']) {
					$nom = $profil['auteur_nom'];
				}
				elseif (isset($profil['auteur_email']) and $profil['auteur_email']) {
					$nom = $profil['auteur_email'];
				}
				elseif (isset($profil['organisation_nom']) and $profil['organisation_nom']) {
					$nom = $profil['organisation_nom'];
				}
				elseif (isset($profil['contact_nom']) and $profil['contact_nom']) {
					$nom = $profil['contact_nom'];
				}
				else {
					$nom = '???';
				}
				// On ajoute la tâche d'import
				if (job_queue_add(
					'importer_csv_profil',
					'Importer le profil "'.$nom.'"',
					array($id_profil, $profil, $envoyer_notification),
					'inc/'
				) > 0){
					$jobs++;
				}
				else{
					$jobs_erreurs++;
				}
			}
			if ($jobs_erreurs) {
				$erreur = _T(
					'profils:message_erreur_importer_jobs',
					array(
						'nb_jobs' => $jobs,
						'nb_erreurs' => $jobs_erreurs,
					)
				);
			}
		} else {
			$erreur = _T('profils:message_erreur_importer_format');
		}
	}

	if (!$erreur) {
		$retours['message_ok'] = _T(
			'profils:message_ok_importer',
			array(
				'nb' => $jobs,
			)
		);
	} else {
		$retours['message_erreur'] = $erreur;
	}
	
	return $retours;
}


/**
 * Détecter le format des colonnes d'un profil
 *
 * @note
 * On ne peut pas détecter les cas (improbables) ou des labels ressembleraient à des formats informatiques.
 * Ou alors s'il y a un mélange de formats.
 * Mais faut pas pousser mémé.
 *
 * @see profils_profil_lire() qui définit ces formats.
 *
 * @param array $colonnes
 * @param int $id_profil
 * @return string
 *     - simple : noms des saisies un peu simplifiés → auteur_truc_muche
 *     - brut   : noms bruts des saisies → auteur[truc][muche]
 *     - label  : labels humains des saisies
 */
function profils_detecter_format_colonnes($colonnes, $id_profil) {
	$format = '';

	// Stratégie : on se repose sur la colonne qui correspond à l'email principal, qui est obligatoire.
	// On transpose le nom dans chaque format prédéfini, puis on cherche sa présence.

	include_spip('inc/profils');
	include_spip('base/abstract_sql');
	$config = unserialize(sql_getfetsel('config', 'spip_profils', 'id_profil='.intval($id_profil)));
	$champ_email_principal = profils_chercher_champ_email_principal($config);        // Ex. : ['auteur', 'email']
	$champ_email_principal_simple = implode('_', $champ_email_principal);            // Ex. : `auteur_email`
	$champ_email_principal_brut = $champ_email_principal[0] . implode('', array_map( // Ex. : `auteur[email]`
		function ($v) {
			return "[$v]";
		},
		array_slice($champ_email_principal, 1)
	));
	if (in_array($champ_email_principal_simple, $colonnes)) {
		$format = 'simple';
	} elseif (in_array($champ_email_principal_brut, $colonnes)) {
		$format = 'brut';
	} else {
		$format = 'label';
	}

	return $format;
}
