<?php
/**
 * Gestion du formulaire de d'exporter de profils depuis un tableur
 *
 * @plugin     Profils
 * @copyright  2018
 * @author     Les Développements Durables
 * @licence    GNU/GPL
 * @package    SPIP\Profils\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/profils');
include_spip('inc/saisies');

function formulaires_exporter_profils_saisies_dist($id_profil) {
	$saisies = array(
		array(
			'saisie' => 'case',
			'options' => array(
				'nom' => 'seulement_colonnes',
				'label_case' => _T('profil:exporter_champ_seulement_colonnes_label_case'),
				'pleine_largeur' => 'oui',
			),
		),
		array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'format_colonnes',
				'label' => _T('profil:exporter_champ_format_colonnes_label'),
				'data' => array(
					'simple' => _T('profil:exporter_champ_format_colonnes_option_simple'),
					'label' => _T('profil:exporter_champ_format_colonnes_option_label'),
				),
				'defaut' => 'simple',
				'pleine_largeur' => 'oui',
			),
		),
		'options' => array(
			'texte_submit' => _T('bouton_telecharger'),
			'inserer_debut' => '<h3 class="titrem">'._T('profil:exporter_titre').'</h3>'
		),
	);

	return $saisies;
}

function formulaires_exporter_profils_traiter_dist($id_profil) {
	refuser_traiter_formulaire_ajax();
	$retours = array();
	
	if ($profil = profils_recuperer_profil($id_profil) and $config = $profil['config']) {
		$colonnes = array();
		$donnees = array();

		// D'abord les colonnes
		// On prend un auteur fictif afin d'avoir les saisies correspondant à l'édition
		$format_colonnes = _request('format_colonnes');
		$ligne = profils_profil_lire(-1, $profil['id_profil'], array('format' => $format_colonnes));
		$colonnes = array_keys($ligne);

		// Si on demande tout, on va charger les données de chaque profil
		if (!_request('seulement_colonnes')) {
			// On récupère tous les auteurs de ce profil
			if ($ids = sql_allfetsel('id_auteur', 'spip_auteurs', 'id_profil = '.$profil['id_profil'])) {
				$ids = array_column($ids, 'id_auteur');
				foreach ($ids as $id_auteur) {
					$ligne = profils_profil_lire($id_auteur, $profil['id_profil']);
					$donnees[] = array_values($ligne);
				}
			}
		}
		
		$exporter_csv = charger_fonction('exporter_csv', 'inc/');
		$exporter_csv($profil['identifiant'], $donnees, ',', $colonnes);
	}
	
	return true;
}
